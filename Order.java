package edu.OSU;

import com.gupysinteractivehealth.Main;

public class Order extends Diner {
    public int burgers_num;
    public int fries_num;
    public int coke;

    @Override
    public String toString() {
        return "Order: "+burgers_num+", "+fries_num+", "+coke;
    }
}
