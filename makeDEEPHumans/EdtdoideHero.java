import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * Energizer Class
 * @author Joseph Lenton
 * @date 06/02/07
 * 
 * The Energizer Class is for making Energizer instances,
 * which when PacMan finds, will tell him to change the Ghosts
 * from normal to danger behaviour modes.
 */
public class Energizer extends Actor
{
    public Energizer()
    {
    }

    public void act()
    {
        //here you can create the behaviour of your object
    }

}
