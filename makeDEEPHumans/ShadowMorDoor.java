/**
 * This code allows you to represent a generic ghost to be used in a simple game.
 */
 
public class ShadowMorDoor extends Enemy {
	public ShadowMorDoor(String name, String description){
		super(name, description, Character.CHAINS); 
	}
}
