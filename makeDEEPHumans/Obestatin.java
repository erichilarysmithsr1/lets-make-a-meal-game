/**
 * This code allows you to represent a generic vampire to be used in a simple game.
 */
 
public class Obestatin extends Enemy {
	public Obestatin(String name, String description){
		super(name, description, Character.TEETH); 
	}
}
