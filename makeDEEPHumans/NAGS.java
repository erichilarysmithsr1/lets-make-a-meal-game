/**
 * This code allows you to represent a generic witch to be used in a simple game.
 */
 
public class NAGS extends Enemy{
	public NAGS(String name, String description){
		super(name, description, Character.SPELL); 
	}
}
