/**
 * Main class of the Java program.
 * This code allows you to create different characters to be used in a simple
 * game. 
 */

 public class NightScout extends Main {

	public static void main (String[] args){
	    System.out.println("Setting up the character");
	    //Example of constructor invocation
	    Character heroNightScout = new Character("NightScout", 10, 5);
	    //We can call a method of a particular Character using:
	    //the name of the object followed by a point and the name of the method
	    heroNightScout.printInfo();
	}
}
