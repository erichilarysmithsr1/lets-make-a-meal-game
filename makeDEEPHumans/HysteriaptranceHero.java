import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * GhostHealer Class
 * @author Joseph Lenton
 * @date 06/02/07
 * 
 * The GhostHealer class is for marking the Ghosts Home.
 * It is where they head to in order to find the Ghost's home
 * when they are dead, and as a marker to state that the
 * Ghost is in their home.
 */
public class GhostHealer extends Actor
{
    public GhostHealer()
    {
    }

    public void act()
    {
        //here you can create the behaviour of your object
    }

}
