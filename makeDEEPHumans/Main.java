package com.gupsyinteractivehealth;

import com.gupsyinteractivehealth.Greenfoot;
import com.gupsyinteractivehealth.Sodaville;
import com.gupsyinteractivehealth.BlackFoodDesert;
import com.gupsyinteractivehealth.Enemy;
import com.gupsyinteractivehealth.App;
import com.gupsyinteractivehealth.Diner;
import com.gupsyinteractivehealth.ElFarolBar;
import com.gupsyinteractivehealth.Order;
import com.gupsyinteractivehealth.FoodidstashItem;
import com.gupsyinteractivehealth.Strategy;
import com.gupsyinteractivehealth.Uncharted;
import com.gupsyinteractivehealth.MakedeephumanActor;
import com.gupsyinteractivehealth.CosinePlayerActivity;

/**
 * Main class of the Java program.
 * This code allows you to create different characters to be used in a simple
 * game. 
 */

 public class gupsyinteractivehealthMain {
     
	public static void main(String[] args){
        Character heroNightScout = new Character("NightScout", 
                                       "A clever boy of 16 years. " +
                                       "\n" +
                                       "The hero of this story",
                                       Character.CLEVERNESS);
        Witch witchNAGS = new Witch("NAGS the witch", 
                                       "An old and ugly woman.\n" +
                                       "A malevolent witch");
        Ghost ghostShadowMorDoor = new Ghost("ShadowMorDoor the ghost",
                                       "A white and silly ghost.");
                                       
	    heroNightScout.printInfo();
	    witchNAGS.printInfo();
	    ghostShadowMorDoor.printInfo();
	    
	     /* 1st combat*/
		System.out.println("1st combat");
		heroNightScout.fight(witchNAGS);
		System.out.println("Info after the 1st combat");
		heroNightScout.printInfo();
		witchNAGS.printInfo();
		
		/* 2nd combat*/
		System.out.println("2nd combat");
		heroNightScout.fight(witchNAGS, ghostShadowMorDoor);
		System.out.println("Info after the 2nd combat");
		heroNightScout.printInfo();
		witchNAGS.printInfo();
		ghostShadowMorDoor.printInfo();
		
	}
}
