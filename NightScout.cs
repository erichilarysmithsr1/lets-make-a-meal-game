using System;
using System.Net.Http;
using System.Threading.Tasks;

public class NightScout : PostmarkDotNet
{
  public static async Task Main(string[] args)
  {
    using (var client = new HttpClient())
    {
      client.DefaultRequestHeaders.Add("Authorization", "Bearer <YOUR_TOKEN_HERE>");
      var request = await client.GetAsync("https://api.dexcom.com/v3/users/self/devices");
      var response = await request.Content.ReadAsStringAsync();

      Console.WriteLine(response);
    }
  }
}
