OverridePusher.java:
package com.contrivancecompanychicago.gupsyinteractivehealth;

import java.lang.Object;
import com.unity3d.player.UnityPlayerActivity;
import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class CosinePlayerActivity extends AppCompatActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    PusherOptions options = new PusherOptions();
    options.setCluster("mt1");

    Pusher pusher = new Pusher("8ad4e366f0e1dc1efe33", options);

    pusher.connect(new ConnectionEventListener() {
      @Override
      public void onConnectionStateChange(ConnectionStateChange change) {
        Log.i("Pusher", "State changed from " + change.getPreviousState() +
            " to " + change.getCurrentState());
      }

      @Override
      public void onError(String message, String code, Exception e) {
        Log.i("Pusher", "There was a problem connecting! " +
          "\ncode: " + code +
          "\nmessage: " + message +
          "\nException: " + e
      );
      }
    }, ConnectionState.ALL);

    Channel channel = pusher.subscribe("my-channel");

    channel.bind("my-event", new SubscriptionEventListener() {
      @Override
      public void onEvent(PusherEvent event) {
        Log.i("Pusher", "Received event with data: " + event.toString());
      }
    });
  }
}
