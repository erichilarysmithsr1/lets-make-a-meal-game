import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * Food Class
 * @author Joseph Lenton
 * @date 06/02/07
 * 
 * Food pills that PacMan eats.
 */
public class Food extends Actor
{
    public Food()
    {
        
    }
    
    /**
     * Be the Food.
     */
    public void act()
    {
        // Be the Food.
    }

}
